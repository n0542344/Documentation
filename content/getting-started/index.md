---
eleventyNavigation:
  key: GettingStarted
  title: Getting Started with Codeberg
  icon: rocket
  order: 10
description: This article shows you how to get started with Codeberg
---
<img src="/assets/images/getting-started/knut.svg" style="float: right; width: 150px; border: none; margin-left: 25px; margin-bottom: 25px;" alt="Knut the Polar Bear">

Hello there!

We're glad you're considering to join Codeberg, the <span style="white-space: nowrap;">community-driven</span> <span style="white-space: nowrap;">non-profit</span> software development platform.

I'm Knut the Polar Bear, and I'm going to guide you through your first steps on Codeberg.

You can start with [finding out more about Codeberg](/getting-started/what-is-codeberg) or by jumping right to [your first steps on Codeberg](/getting-started/first-steps).

Welcome to Codeberg! 😊