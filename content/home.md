---
permalink: /
---
Welcome to the Codeberg Documentation pages!

<span class="wide-only">Please choose a section from the main menu on the left.</span>
<span class="narrow-only">Please choose a section from the main menu, which you can access by clicking/touching the three stripes at the top right.</span>

If you're new to Codeberg, consider reading the [Getting Started Guide](/getting-started).
