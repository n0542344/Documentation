---
eleventyNavigation:
  key: Git
  title: Working with Git Repositories
  icon: code-branch
  order: 10
---

On these pages, you will learn how to use the Git version control system
with Codeberg.

There are 3 ways to interact with a Git repository hosted on Codeberg:
1. [via the SSH protocol](/git/clone-commit-via-ssh)
2. [via the HTTPS protocol](/git/clone-commit-via-http)
3. [Using the website](/git/clone-commit-via-web/)

Option 1 and 2 require a Git client of your choice [installed on your local system](/getting-started/install-git/).

We recommend the use of the [SSH protocol](https://en.wikipedia.org/wiki/Secure_Shell_Protocol).   
It offers improved security through key-based access (stronger protection than a regular password) and better usability (no need to provide credentials on every Git action).
