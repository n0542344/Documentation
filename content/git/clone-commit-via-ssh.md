---
eleventyNavigation:
  key: CloneCommitViaSSH
  title: Clone & Commit via SSH
  parent: Git
  order: 40
---

Before you are able to access Git repositories via SSH you need to [add an SSH key to your account](/security/ssh-key).

> **Warning**   
> Please make sure that before connecting to Codeberg via SSH,
> you have [verified Codeberg's SSH fingerprint](/security/ssh-fingerprint)!

The user in these examples is `knut` and its repository is `examples`. The repository was created via the Codeberg.org website including a `README.md` file.

## Clone the repo
```bash
~$ git clone git@codeberg.org:knut/examples.git
Enter passphrase for key '/home/knut/.ssh/id_rsa': ****
Cloning into 'examples'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
```

## Modify and commit to local repository
Modify an existing file

```bash
~$ cd example
~/examples$ vim README.md
```

Commit changes to local repository.

```bash
~/examples$ git commit -am 'test'
[master 10074d7] test
 1 file changed, 2 insertions(+), 1 deletion(-)
```

## Synchronize your local repository with the remote one
```bash
~/examples$ git push
Enter passphrase for key '/home/knut/.ssh/id_rsa': ****
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 266 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To https://codeberg.org/knut/examples.git
   662e04e..10074d7  master -> master
```
